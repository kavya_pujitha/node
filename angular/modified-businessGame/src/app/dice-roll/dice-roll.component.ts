import { Component, OnInit } from '@angular/core';
import { DiceRollService } from '../dice-roll.service';

@Component({
  selector: 'app-dice-roll',
  templateUrl: './dice-roll.component.html',
  styleUrls: ['./dice-roll.component.css']
})
export class DiceRollComponent implements OnInit {
  diceRollValue;
  constructor(private diceRoll:DiceRollService) { 
  }

  ngOnInit() {
  }

  handleClick(){
    // this.diceRoll.getDiceRoll().subscribe(data => {
    //   this.diceRollValue = data;
    // })

    this.diceRollValue = this.diceRoll.getDiceRoll()
    //this.diceRollValue = Math.floor(Math.random() * (12 - 2 + 1) + 2);
  }

}
