import { Injectable } from '@angular/core';
import {Player} from './Player'
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayersDataService {
  PLAYERS:Player[];
  constructor() {
    this.PLAYERS = []
   }

  getPlayers():Observable<Player[]>{
    this.PLAYERS.push({name:"player1",position:0,amount:2000,numberOfChances:10})
    this.PLAYERS.push({name:"player2",position:0,amount:2000,numberOfChances:10})
    this.PLAYERS.push({name:"player3",position:0,amount:2000,numberOfChances:10})
    return of(this.PLAYERS);
  }

  getUpdatedPlayers():Observable<Player[]>{
    return of(this.PLAYERS)
  }

  updatePlayer(currentPlayer:Player): void{
    this.PLAYERS.forEach(function(player:Player,index:number){
      if(player.name===currentPlayer.name){
        this.PLAYERS[index] = currentPlayer;
        return;
      }
    })
  }
}
