import { Injectable } from '@angular/core';
import { Player } from './Player';
import { Observable,of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerDetailChangeService {

  constructor() { }

public changeAmount(player:Player,modifyAmount: number): Observable<Player> {
    return of({
      ...player,
      amount : player.amount + modifyAmount
    })
}

public capableToBuyHotel(player:Player): Observable<boolean> {
    return of(player.amount >= 200);
}

public setPosition(player:Player, changePosition:number):Observable<Player> {
    return of({
      ...player,
      position:changePosition
    })
}

public getPosition(player:Player): Observable<number> {
    return of(player.position);
}

public displayAmount(player:Player): void {
    console.log(`Amount is ${player.amount}`);
} 
}
