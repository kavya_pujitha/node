import { Component, OnInit } from '@angular/core';
import {Cell} from '../cell'
import { BoardDataService } from '../board-data.service'

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  boardCells : Cell[];
  numberOfCells: number;
  position: number = 1;

  constructor(private boardService:BoardDataService){}
  
  ngOnInit() {
    this.boardService.getBoard().subscribe();
  }  
}

