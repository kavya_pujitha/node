import { TestBed } from '@angular/core/testing';

import { PlayerDetailChangeService } from './player-detail-change.service';

describe('PlayerDetailChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlayerDetailChangeService = TestBed.get(PlayerDetailChangeService);
    expect(service).toBeTruthy();
  });
});
