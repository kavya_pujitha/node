import { Injectable } from '@angular/core';
import { Cell} from './cell'
import {Player} from './Player'
import {PlayerDetailChangeService} from './player-detail-change.service'
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CellService {

  constructor(private playerDetailChangeService:PlayerDetailChangeService) { }

  createJailCell(): Observable<Cell>{
    return of({ type: "jail"
    // land(player:Player):Player{
    //   let newPlayer : Player;
    //   this.playerDetailChangeService.changeAmount(player,-150).subscribe(function(player:Player){
    //     this.newPlayer = player;
    //   })
    //   return newPlayer;
    // }
  })
  }

  createEmptyCell(): Observable<Cell>{
    return of({ type: "empty"
    // land(player:Player):Player{
    //   return player;
    // }
  })
  }

  createTreasureCell(): Observable<Cell>{
    return of({ type: "treasure"
    // land(player:Player):Player{
    //   let newPlayer : Player;
    //   this.playerDetailChangeService.changeAmount(player,200).subscribe(function(player:Player){
    //     this.newPlayer = player;
    //   })
    //   return newPlayer;
    // }
  })
  }

}



