import { Injectable } from '@angular/core';
import { Observable,of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiceRollService {

  constructor() { }

  getDiceRoll():Observable<number>{
    return of(Math.floor(Math.random() * (12 - 2 + 1) + 2));
  }

}
