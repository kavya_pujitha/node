import { Injectable } from '@angular/core';
import { CellService } from './cell.service';
import { Cell } from './cell';
import { Observable, of } from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class BoardDataService {
  BOARDCELLS:Cell[];

  constructor(private cellService:CellService) {
    this.BOARDCELLS = [];
  }

 getBoard(): Observable<void>{
  this.BOARDCELLS.push({type: "emp"})

  this.cellService.createEmptyCell().subscribe(cell => {
    console.log(cell)
    this.BOARDCELLS.push(cell)
  })
  
  this.cellService.createJailCell().subscribe(cell => {
    console.log(cell)
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createEmptyCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createTreasureCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createEmptyCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createJailCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createJailCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createEmptyCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createEmptyCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createTreasureCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createEmptyCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })

  this.cellService.createJailCell().subscribe(cell => {
    this.BOARDCELLS.push(cell)
  })
  return of();
  //return of(this.BOARDCELLS);
 }

 getCellType(position:number):Observable<string>{
   return of(this.BOARDCELLS[position].type);
 }
}
