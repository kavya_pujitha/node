export interface Player{
    name: string;
    position: number;
    amount: number;
    numberOfChances: number;
}