import { Component, OnInit, Input } from '@angular/core';
import { BoardDataService } from '../board-data.service';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit {
  cellType:string;
  @Input() position:number;
  constructor(private boardDataService:BoardDataService) { }

  ngOnInit() {
    console.log(this.position)
    this.boardDataService.getCellType(this.position).subscribe(function(type:string){
      this.cellType = type
      console.log("type is " + this.cellType)
    })
  }
}
