import { Component, OnInit } from '@angular/core';
import { Player } from '../Player';
import { PlayersDataService } from '../players-data.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.css']
})
export class PlayerDetailComponent implements OnInit {
  players: Player[]
  constructor(private playersDataService: PlayersDataService) { 
  }

  ngOnInit() {
    this.playersDataService.getUpdatedPlayers().subscribe(function(playersData:Player[]){
      this.players = playersData;
      console.log(this.players);
    })
  }
}
