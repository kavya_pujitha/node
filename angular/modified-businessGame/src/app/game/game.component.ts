import { Component, OnInit } from '@angular/core';
import {Player} from '../Player'
import { PlayersDataService } from '../players-data.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  players:Player[];
  diceRoll:number;
  constructor(private playerDataService:PlayersDataService) { }

  ngOnInit() {
    this.playerDataService.getPlayers().subscribe(
      function(playersList:Player[]){
        this.players = playersList;
        console.log(this.players)
      }
    )
  }




}
