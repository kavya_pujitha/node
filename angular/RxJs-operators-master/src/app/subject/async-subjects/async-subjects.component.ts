import { Component, OnInit } from '@angular/core';
import { AsyncSubject } from 'rxjs';

@Component({
  selector: 'app-async-subjects',
  templateUrl: './async-subjects.component.html',
  styleUrls: ['./async-subjects.component.css']
})
export class AsyncSubjectsComponent implements OnInit {
  messages: String[] = [];
  constructor() { }

  ngOnInit() {
    const subject = new AsyncSubject();
    subject.subscribe((data) => {this.messages.push('Subscriber 1: ' + data); });
    subject.next(1);
    subject.next(2);
    subject.next(3);
    subject.subscribe((data) => {this.messages.push('Subscriber 2: ' + data); });
    subject.next(4);
    subject.complete();

  }

}
