import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-dice-roll',
  templateUrl: './dice-roll.component.html',
  styleUrls: ['./dice-roll.component.css']
})

export class DiceRollComponent implements OnInit {
  @Input() disableRoll: boolean;
  //@Input() displayDiceValue: boolean;
  @Output() numberEvent = new EventEmitter<number>();
  diceRollValue:number;

  constructor() {
  }

  ngOnInit() {
  }

  handleClick(event:Event){
    //this.displayDiceValue = true;
    this.diceRollValue = Math.floor(Math.random() * (12 - 2 + 1) + 2);
    this.numberEvent.emit(this.diceRollValue);
  }

}
