import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture } from '@angular/core/testing';
import { PlayersDataService } from './players-data.service'
import { Player } from './player'

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  
  it('should create app',()=>{
    expect(component).toBeTruthy();
  })

  it('should render title', () => {
      const titleElement = fixture.debugElement.nativeElement.querySelector('h1')
      expect(titleElement.textContent).toEqual('Business Game')
  })

  it('should render start game button',()=>{
    const buttonElement = fixture.debugElement.nativeElement.querySelector('button.startGameButton')
    expect(buttonElement).toBeTruthy();
  })

  it('should start the game when start button is clicked', () => {
    const startSpy = spyOn(component, 'start');
    let button = fixture.debugElement.nativeElement.querySelector('button.startGameButton')
    button.click();
    expect(startSpy).toHaveBeenCalled();
  });

  it('should initialise when start is called', () => {  
    let service:PlayersDataService;
    service = new PlayersDataService();

    let spy:any = spyOn(service, 'getPlayers').and.returnValue(
      [
        new Player('abc')
      ]
    );  
    component.players = spy;

    let event:Event = new Event('');
    component.start(event);

    expect(component.disableRoll).toBe(false);
    expect(component.currentPlayer).toEqual(spy[0]);
  })

  // it('should assign event', () => {
  //   const event= 'my event';
  //   component.receiveDiceRoll(event);
  //   expect(component.diceRoll).toEqual(event)
  // })
});
