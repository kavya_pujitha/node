import * as cells from './board/cell'
export class Player {
    name: string;
    position: number;
    amount: number;
    numberOfChances: number;
    hotelOwnerCells: cells.HotelCell[];

    constructor(name: string){
        this.name = name;
    }

    public changeAmount(modifyAmount: number): void {
        this.amount += modifyAmount;
    }

    public ownsHotel(hotel: cells.Cell): boolean {
        this.hotelOwnerCells.forEach(function (currentHotel: cells.Cell) {
            if (currentHotel === hotel) {
                return true;
            }
        })
        return false;
    }

    public capableToBuyHotel(): boolean {
        return this.amount >= 200;
    }

    public buyHotel(hotel: cells.HotelCell) {
        this.hotelOwnerCells.push(hotel);
    }

    public setPosition(currentRollValue: number, boardLength: number) {
        this.position = (this.position + currentRollValue) % boardLength;
    }

    public getPosition(): number {
        return this.position;
    }

    public displayAmount(): void {
        console.log(`Amount is ${this.amount}`);
    }

}