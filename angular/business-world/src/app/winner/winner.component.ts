import { Component, OnInit } from '@angular/core';
import { Player } from '../player';
import { PlayersDataService } from '../players-data.service';

@Component({
  selector: 'app-winner',
  templateUrl: './winner.component.html',
  styleUrls: ['./winner.component.css']
})
export class WinnerComponent implements OnInit {
  players : Player[];
  winner: string;
  constructor(private playersDataService:PlayersDataService) { }

  ngOnInit() {
    this.players = this.playersDataService.getPlayers();
    this.getWinner();
  }

  getWinner(){    
    let maxAmount = -1;
    let maxAmountPlayer: string;
    this.players.forEach(function(player){
      console.log(player);
      console.log("Enter get winner loop");
        if(player.amount>maxAmount){
          maxAmount = player.amount;
          maxAmountPlayer = player.name;
          console.log(maxAmountPlayer)
        }
    });
    this.winner = maxAmountPlayer;
    console.log("winner is "+ this.winner)
  }
  
}
