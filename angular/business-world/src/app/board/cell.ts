import { Player } from '../player'
import {PlayersDataService} from '../players-data.service'
import { OnInit } from '@angular/core';

export interface Cell{
    type:string;
    land(currentPlayer:Player):void;
}

export class JailCell implements Cell{
    type:string= "jail";
    land(currentPlayer:Player):void{
        currentPlayer.changeAmount(-150);
    }
}

export class TreasureCell implements Cell{
    type:string= "treasure";

    land(currentPlayer:Player):void{
        currentPlayer.changeAmount(200);
    }
}

export class EmptyCell implements Cell{
    type:string= "empty";

    land(currentPlayer:Player):void{
        currentPlayer.changeAmount(0);
    }
}

export class HotelCell implements Cell,OnInit{ 

    players:Player[];

    constructor(private playerService:PlayersDataService){
        
    }

    ngOnInit(){
        this.players = this.playerService.getPlayers();
    }
    type:string= "hotel";

    land(currentPlayer:Player):void{
        let owner : Player = this.preOwned()
        if(!owner){
            if(currentPlayer.capableToBuyHotel()){
                currentPlayer.changeAmount(-200);
                currentPlayer.buyHotel(this);
            }
        }
        else if(owner !== currentPlayer){
            currentPlayer.changeAmount(-50);
            owner.changeAmount(50);
        }
    }
    preOwned():Player{
        let owner: Player =null;
        this.playerService.getPlayers().forEach(
            function(otherPlayer:Player){
                if(otherPlayer.ownsHotel(this)){
                    owner = otherPlayer;
                }
            }
        )
        return owner;
    }
}