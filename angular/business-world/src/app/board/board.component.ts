import { Component, OnInit } from '@angular/core';
import { BoardDataService } from '../board-data.service'
import { Cell } from './cell';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  boardCells : Cell[];
  numberOfCells: number;
  position: number = 1;

  constructor(private boardService:BoardDataService){}
  
  ngOnInit() {
    this.boardCells = this.boardService.getBoard();
  }  
}
