import { Injectable } from '@angular/core';
import * as cells from './board/cell'
import {PlayersDataService} from './players-data.service'

@Injectable({
  providedIn: 'root'
})

export class BoardDataService{

  constructor(private playerService:PlayersDataService) {
   }

  getBoard(){
    return [new cells.EmptyCell(),
  new cells.HotelCell(this.playerService),
  new cells.TreasureCell(),
  new cells.JailCell(),
  new cells.HotelCell(this.playerService),
  new cells.EmptyCell(),
  new cells.TreasureCell(),
  new cells.HotelCell(this.playerService),
  new cells.EmptyCell(),
  new cells.TreasureCell(),
  new cells.JailCell(),
  new cells.EmptyCell()];
  }
}
