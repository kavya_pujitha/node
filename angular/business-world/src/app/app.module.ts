import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { CellComponent } from './board/cell/cell.component';
import { DiceRollComponent } from './dice-roll/dice-roll.component';
import { PlayerDetailsComponent } from './player-details/player-details.component';
import { UpperCaseCustomPipe } from './board/cell/cell.pipe';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    CellComponent,
    DiceRollComponent,
    PlayerDetailsComponent,
    UpperCaseCustomPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
