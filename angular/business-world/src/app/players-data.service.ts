import { Injectable } from '@angular/core';
import { Player } from './player';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayersDataService {
  constructor() { }

  getPlayers():Observable<Player[]>{
    return of([
      new Player("player1"),
      new Player("player2")
    ]);
  }
}
