import { Component, OnInit, Input } from '@angular/core';
import { Player } from './player';
import { Cell } from './board/cell';
import {PlayersDataService} from './players-data.service'
import { BoardDataService } from './board-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title: string = 'business-world';
  diceRoll: number;
  players: Player[];
  currentPlayer: Player;
  index: number =0;
  disableRoll: boolean = true;
  boardData:Cell[];
  //displayDiceValue: boolean = false;

  constructor(private playersService:PlayersDataService,private boardService:BoardDataService){

  }
  ngOnInit() {
    this.boardData = this.boardService.getBoard();
    this.playersService.getPlayers().subscribe(data =>{
      this.players = data;
    });
  }

  start(event:Event):void{
    this.disableRoll = false;
    this.currentPlayer = this.players[0];
    this.players.forEach(function(player){
      player.amount=2000;
      player.position=0;
      player.numberOfChances=2;
      player.hotelOwnerCells=[];
    })
  }

  receiveDiceRoll($event){
    this.diceRoll = $event;
    this.currentPlayer.numberOfChances--;
    this.currentPlayer.setPosition(this.diceRoll, this.boardData.length);
    let currentCell: Cell = this.boardData[this.currentPlayer.position];
    currentCell.land(this.currentPlayer);
    console.log(this.currentPlayer.position);
    this.index++;
    this.currentPlayer=this.players[this.index%this.players.length] 
    if(this.gameEndCondition()){
      this.disableRoll = true;
    }
  }

  gameEndCondition(){
    let condition: boolean = true;
    this.players.forEach(function find(player){
      if(player.numberOfChances>0){
        condition = false;
      }
    });
    return condition;
  }
}


