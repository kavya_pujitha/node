import { Component, OnInit, Input } from '@angular/core';
import {Hero} from "../hero"
import {HeroService} from "../hero.service"

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes:Hero[];
  selectedHero:Hero;
  hero:Hero = {
    name:"WindStorm",
    id:1,
  };
  constructor(private heroService:HeroService) {
  }

  getHeroes(): void {
     this.heroService.getHeroes().subscribe(
       function(data){
         this.heroes = data;
       }
     )
  }

  ngOnInit() {
    this.getHeroes();
  }

  onselect(hero:Hero):void{
    this.selectedHero = hero;
  }

}
