//splitting string into array of characters

let word = "thoughtworks";
let characterArray = [...word];
console.log(characterArray);

//concatenation of strings

let word1 = "thought";
let word2 = "works";
let combinedWord = [...word1, ...word2];
console.log(word);

//pushing an array into middle of another array
let arr1 = [3, 4];
let combinedArray = [1, 2, ...arr1, 5];
console.log(combinedArray);

//updating the existing array
const student1 = { name: 'abc', class: '1' }
const student2 = { name: 'def', class: '1' }
const student3 = { name: 'ghf', class: '1' }
const student = [student1, student2, student3];
const studentCopy = [...student];
studentCopy.find(student=>student.name=='def').class='2';
console.log("updating the existing array")
console.log(student);
console.log(studentCopy);

//updating the existing array
const student01 = { name: 'abc', class: '1' }
const student02 = { name: 'def', class: '1' }
const student03 = { name: 'ghf', class: '1' }
const student0 = [student01, student02, student03]
const studentCopy0 = [...student0.slice(0, 1), Object.assign(...student0.slice(1, 2), {class:'2'}), ...student0.slice(2, 3)];
console.log("updating the existing array")
console.log(studentCopy0);
console.log(student0);

//copying updated contents to a new array
const student001 = { name: 'abc', class: '1' }
const student002 = { name: 'def', class: '1' }
const student003 = { name: 'ghf', class: '1' }
const student00 = [student001, student002, student003]
const studentCopy00 = [...student00.slice(0, 1), {...student00[1], class: '2'}, ...student00.slice(2, 3)];
console.log("copying updated contents to a new array")
console.log(studentCopy00);
console.log(student00);