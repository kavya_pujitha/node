let arr = [1, 2, 3, 4, 5, 6, 7, 8];

Array.prototype.myFilter = function (myFunction) {
  let newArray = [];
  this.forEach((item) => {
    if (myFunction(item)) {
      newArray=[...newArray,item];
    }
  })
  return newArray;
}

let evenArray = arr.myFilter((num) => num % 2 == 0);
console.log(evenArray);