function main() {
    //simple function type params
    function add(num1, num2) {
        return num1 + num2;
    }
    var sum = add(2, 3);
    console.log(sum);
    //object declaration and then initializing
    var employee;
    employee = {
        id: 123,
        name: "abc"
    };
    //string template
    console.log("employee id is " + employee.id + " and name is " + employee.name);
    console.log(typeof (employee.id.toString()));
    //Number class
    var num = new Number(123);
    console.log(num.valueOf());
    //tuple
    var tuple = [[1, "name"]];
    tuple.push([1, "bca"]);
    console.log(tuple);
    //enum
    var PrintMedia;
    (function (PrintMedia) {
        PrintMedia["Newspaper"] = "NEWSPAPER";
        PrintMedia["Magazine"] = "MAGAZINE";
    })(PrintMedia || (PrintMedia = {}));
    var PrintMediaNumber;
    (function (PrintMediaNumber) {
        PrintMediaNumber[PrintMediaNumber["Newspaper"] = 0] = "Newspaper";
        PrintMediaNumber[PrintMediaNumber["Magazine"] = 1] = "Magazine";
    })(PrintMediaNumber || (PrintMediaNumber = {}));
    console.log(PrintMedia["Magazine"]);
    console.log(PrintMediaNumber[0]);
    console.log(PrintMediaNumber["Magazine"]);
    //function return type (as enum)
    function getMedia(media) {
        if (media === "Newspaper") {
            return PrintMedia.Newspaper;
        }
    }
    var out = getMedia("Newspaper");
    console.log(out);
    console.log(PrintMedia);
    //type inference
    var name = "abc";
    console.log(typeof (name));
    var emp = /** @class */ (function () {
        function emp() {
        }
        return emp;
    }());
    var emp1 = {};
    var emp2 = emp1;
    emp2.name = "abc";
    emp2.id = 123;
    console.log(typeof (emp1) + " " + typeof (emp2));
}
main();
