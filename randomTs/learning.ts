function main(){

  //simple function type params
  function add(num1 : number, num2 : number):number{
    return num1 + num2;
  }

  let sum = add(2,3);
  console.log(sum)

  //object declaration and then initializing
  let employee : {
    id : number,
    name : string
  }

  employee = {
    id : 123,
    name : "abc"
  }

  //string template
  console.log(`employee id is ${employee.id} and name is ${employee.name}`)
  console.log(typeof(employee.id.toString()))

  //Number class
  let num : Number = new Number(123)
  console.log(num.valueOf())

  //tuple
  var tuple : [number, string][] = [[1, "name"]]
  tuple.push([1, "bca"]);
  console.log(tuple)

  //enum
  enum PrintMedia{
    Newspaper = "NEWSPAPER",
    Magazine = "MAGAZINE"
  }

  enum PrintMediaNumber{
    Newspaper,
    Magazine
  }

  console.log(PrintMedia["Magazine"]);

  console.log(PrintMediaNumber[0]);
  console.log(PrintMediaNumber["Magazine"]);

  //function return type (as enum)
  function getMedia(media:string):PrintMedia{
    if(media==="Newspaper"){
      return PrintMedia.Newspaper;
    }
  }

  let out:PrintMedia = getMedia("Newspaper")
  console.log(out)
  console.log(PrintMedia)

  //type inference
  let name = "abc"
  console.log(typeof(name))
  
  //type assertion
  interface emp{
    name : string;
    id : number;
  }
  let emp1 = {};
  let emp2 = <emp> emp1;
  emp2.name = "abc"
  emp2.id = 123
  console.log(typeof(emp1) + " " + typeof(emp2))

  
}
main()