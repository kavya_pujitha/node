"use strict";
exports.__esModule = true;
var player = /** @class */ (function () {
    function player(initialAmount) {
        this.amount = initialAmount;
        this.position = 0;
        this.hotelOwnerCells = [];
    }
    player.prototype.changeAmount = function (modifyAmount) {
        this.amount += modifyAmount;
    };
    player.prototype.ownsHotel = function (hotel) {
        this.hotelOwnerCells.forEach(function (currentHotel) {
            if (currentHotel === hotel) {
                return true;
            }
        });
        return false;
    };
    player.prototype.capableToBuyHotel = function () {
        return this.amount >= 200;
    };
    player.prototype.buyHotel = function (hotel) {
        this.hotelOwnerCells.push(hotel);
    };
    player.prototype.setPosition = function (currentRollValue, boardLength) {
        this.position = (this.position + currentRollValue) % boardLength;
    };
    player.prototype.getPosition = function () {
        return this.position;
    };
    player.prototype.displayAmount = function () {
        console.log("Amount is " + this.amount);
    };
    return player;
}());
exports.player = player;
