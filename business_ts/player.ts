import {hotelCell} from "./cell"
import {cell} from "./cell"

export class player{
    private amount:number;
    private position:number;
    private hotelOwnerCells:cell[];

    constructor(initialAmount:number){
        this.amount = initialAmount;
        this.position = 0;
        this.hotelOwnerCells = [];
    }

    public changeAmount(modifyAmount:number):void{
        this.amount+=modifyAmount;
    }

    public ownsHotel(hotel:hotelCell):boolean{
        this.hotelOwnerCells.forEach(function(currentHotel:cell){
            if(currentHotel === hotel){
                return true;
            }
        })
        return false;
    }

    public capableToBuyHotel():boolean{
        return this.amount>=200;
    }

    public buyHotel(hotel:hotelCell){
        this.hotelOwnerCells.push(hotel);
    }

    public setPosition(currentRollValue:number,boardLength:number){
        this.position = (this.position+currentRollValue)%boardLength;
    }

    public getPosition():number{
        return this.position;
    }

    public displayAmount():void{
        console.log(`Amount is ${this.amount}`);
    }
}
