import * as cells from "./cell"
import { player } from "./player"

class game {
    private board: cells.cell[] = [
        new cells.jailCell(),
        new cells.emptyCell(),
        new cells.treasureCell(),
        new cells.jailCell(),
        new cells.hotelCell(),
        new cells.emptyCell(),
        new cells.treasureCell(),
        new cells.hotelCell(),
        new cells.hotelCell(),
        new cells.jailCell()
    ]

    private players: player[] = [
        new player(2000),
        new player(2000),
        new player(2000)
    ]

    public getPlayers():player[]{
        return this.players;
    }

    private randomNumberGenerator():number {
        return Math.floor(Math.random() * (12 - 2 + 1) + 2);
    }

    public start(): void {
        let numberOfPlayers: number = this.players.length
        let totalChances:number = numberOfPlayers * 10;

        for (let i = 0; i < totalChances; i++) {
            let currentPlayer: player = this.players[i % numberOfPlayers]
            let currentRollValue: number = this.randomNumberGenerator();
            currentPlayer.setPosition(currentRollValue, this.board.length);
            let currentCell: cells.cell = this.board[currentPlayer.getPosition()];
            currentCell.land(currentPlayer);
            console.log(currentCell)
        }
        
        this.players.forEach(function(eachPlayer:player){
            eachPlayer.displayAmount();
        })
    }

}

export let myGame:game= new game();
myGame.start();