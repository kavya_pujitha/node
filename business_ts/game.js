"use strict";
exports.__esModule = true;
var cells = require("./cell");
var player_1 = require("./player");
var game = /** @class */ (function () {
    function game() {
        this.board = [
            new cells.jailCell(),
            new cells.emptyCell(),
            new cells.treasureCell(),
            new cells.jailCell(),
            new cells.hotelCell(),
            new cells.emptyCell(),
            new cells.treasureCell(),
            new cells.hotelCell(),
            new cells.hotelCell(),
            new cells.jailCell()
        ];
        this.players = [
            new player_1.player(2000),
            new player_1.player(2000),
        ];
    }
    game.prototype.getPlayers = function () {
        return this.players;
    };
    game.prototype.randomNumberGenerator = function () {
        return Math.floor(Math.random() * (12 - 2 + 1) + 2);
    };
    game.prototype.start = function () {
        var numberOfPlayers = this.players.length;
        var totalChances = numberOfPlayers * 2;
        for (var i = 0; i < totalChances; i++) {
            var currentPlayer = this.players[i % numberOfPlayers];
            var currentRollValue = this.randomNumberGenerator();
            currentPlayer.setPosition(currentRollValue, this.board.length);
            var currentCell = this.board[currentPlayer.getPosition()];
            currentCell.land(currentPlayer);
            console.log(currentCell);
        }
        this.players.forEach(function (eachPlayer) {
            eachPlayer.displayAmount();
        });
    };
    return game;
}());
exports.myGame = new game();
exports.myGame.start();
