import {player} from "./player"
import {myGame} from "./game"

export interface cell{
    land(currentPlayer:player):void;
}

export class jailCell implements cell{
    land(currentPlayer:player):void{
        currentPlayer.changeAmount(-150);
    }
}

export class treasureCell implements cell{
    land(currentPlayer:player):void{
        currentPlayer.changeAmount(200);
    }
}

export class emptyCell implements cell{
    land(currentPlayer:player):void{
        currentPlayer.changeAmount(0);
    }
}

export class hotelCell implements cell{ 
    land(currentPlayer:player):void{
        let owner : player = this.preOwned()
        if(!owner){
            if(currentPlayer.capableToBuyHotel()){
                currentPlayer.changeAmount(-200);
                currentPlayer.buyHotel(this);
            }
        }
        else if(owner !== currentPlayer){
            currentPlayer.changeAmount(-50);
            owner.changeAmount(50);
        }
    }
    preOwned():player{
        let owner: player =null;
        myGame.getPlayers().forEach(
            function(otherPlayer:player){
                if(otherPlayer.ownsHotel(this)){
                    owner = otherPlayer;
                }
            }
        )
        return owner;
    }

}