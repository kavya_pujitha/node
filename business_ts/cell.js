"use strict";
exports.__esModule = true;
var game_1 = require("./game");
var jailCell = /** @class */ (function () {
    function jailCell() {
    }
    jailCell.prototype.land = function (currentPlayer) {
        currentPlayer.changeAmount(-150);
    };
    return jailCell;
}());
exports.jailCell = jailCell;
var treasureCell = /** @class */ (function () {
    function treasureCell() {
    }
    treasureCell.prototype.land = function (currentPlayer) {
        currentPlayer.changeAmount(200);
    };
    return treasureCell;
}());
exports.treasureCell = treasureCell;
var emptyCell = /** @class */ (function () {
    function emptyCell() {
    }
    emptyCell.prototype.land = function (currentPlayer) {
        currentPlayer.changeAmount(0);
    };
    return emptyCell;
}());
exports.emptyCell = emptyCell;
var hotelCell = /** @class */ (function () {
    function hotelCell() {
    }
    hotelCell.prototype.land = function (currentPlayer) {
        var owner = this.preOwned();
        if (!owner) {
            if (currentPlayer.capableToBuyHotel()) {
                currentPlayer.changeAmount(-200);
                currentPlayer.buyHotel(this);
            }
        }
        else if (owner !== currentPlayer) {
            currentPlayer.changeAmount(-50);
            owner.changeAmount(50);
        }
    };
    hotelCell.prototype.preOwned = function () {
        var owner = null;
        game_1.myGame.getPlayers().forEach(function (otherPlayer) {
            if (otherPlayer.ownsHotel(this)) {
                owner = otherPlayer;
            }
        });
        return owner;
    };
    return hotelCell;
}());
exports.hotelCell = hotelCell;
