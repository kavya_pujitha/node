var assert = require('assert');
var app = require('../app');

describe('Board', function () {
  it('check if right cell is called after the roll', function () {
    var result = app.cellOnBoard(2);
    assert.equal(result, "empty");
  })

  it('check if the correct position is called after one round of board is completed', function () {
    var positionOnBoard = app.positionOnBoardAfterRoll(15, 4);
    assert.equal(positionOnBoard, 3);
  })
})

describe('Person Amount', function () {
  it("Player's amount deducted when he lands on jail cell", function () {
    var player1 = new app.player();
    player1.amount = 1000;
    app.changeAmount(player1, "jail");
    assert.equal(player1.amount, 850);
  })
  it("Player's amount added when he lands on treasure cell", function () {
    var player1 = new app.player();
    player1.amount = 1000;
    app.changeAmount(player1, "treasure");
    assert.equal(player1.amount, 1200);
  })
  it("Player's amount added when he buys a hotel", function () {
    var player1 = new app.player();
    player1.amount = 1000;
    app.changeAmount(player1, "hotel");
    assert.equal(player1.amount, 800);
  })
  //Mocking
})

